<?php
/**
 * @package Siwa
 */
/*
Plugin Name: Siwa
Plugin URI: http://siwa.beltex.nl/
Description: Used by millions, Siwa is quite possibly the best way in the world to <strong>protect your blog from comment and trackback spam</strong>. It keeps your site protected from spam even while you sleep. To get started: 1) Click the "Activate" link to the left of this description, 2) <a href="http://akismet.com/get/">Sign up for an Siwa API key</a>, and 3) Go to your Siwa configuration page, and save your API key.
Version: 1.0.0
Author: Automattic
Author URI: http://automattic.com/wordpress-plugins/
License: GPLv2 or later
Text Domain: Siwa
*/
defined('ABSPATH') or die("No direct access allowed!");

define( 'SIWA_VERSION', '1.0.0' );
define( 'SIWA__MINIMUM_WP_VERSION', '1.0' );
define( 'SIWA__PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'SIWA__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

require_once( SIWA__PLUGIN_DIR . 'class.siwa.php' );
register_activation_hook( __FILE__, array( 'Siwa', 'set_navigation' ) );
register_activation_hook( __FILE__, array( 'Siwa', 'add_menu_items' ) );
register_deactivation_hook( __FILE__, array( 'Siwa', 'remove_menu_items' ) );
register_deactivation_hook( __FILE__, array( 'Siwa', 'remove_navigation' ) );
add_filter('pre_wp_nav_menu',array('Siwa','siwa_filter_nav_menu'),10,2);
add_action('wp_head', array('Siwa','add_siwa_script'));
require_once( SIWA__PLUGIN_DIR . 'class.siwa-widget.php' );

if ( is_admin() ) {
	require_once( SIWA__PLUGIN_DIR . 'class.siwa-admin.php' );
	$my_settings_page = new Siwa_Admin();	
}




require_once( SIWA__PLUGIN_DIR . 'class.siwa-template.php' );




?>