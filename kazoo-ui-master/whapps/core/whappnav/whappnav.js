winkstart.module('core', 'whappnav', {
        css: [
            'css/whappnav.css'
        ],

        templates: {
            whapp: 'tmpl/whapp.html',
            whapp_divider: 'tmpl/whapp_divider.html',
            module: 'tmpl/module.html',
            module_divider: 'tmpl/module_divider.html',
            category: 'tmpl/category.html'
        },

        subscribe: {
            'whappnav.add': 'add',
            'whappnav.subnav.add': 'sub_add',
            'whappnav.subnav.show': 'show_menu',
            'whappnav.subnav.hide': 'hide_menu',
            'whappnav.subnav.disable' : 'disable_whapp',
            'whappnav.subnav.enable': 'enable_whapp'
        },

        targets: {
            ///nav_bar: '#ws-topbar .whapps'
						nav_bar: '#menu-siwa-navigation'
        }
    },

    function() {
			var THIS = this,
					inserted = false,
					whapp_list_html = $(THIS.config.targets.nav_bar),
					curr_module,
					dat = new Array();
					/*whapp_list_html.each(function(index, element) {
						whapp_html = element;
					});*/
			$('li.siwa-menu').mouseover(function(e) {
				if($(this).hasClass('dropdown') && $('.sub-menu').css('display')=='none'){
       		$('.sub-menu').css('display','block'); 
				}
      });
			
			$('.sub-menu').mouseout(function(e) {
       	$('.sub-menu').css('display','none'); 
      });
			whapp_html = $('#menu-siwa-navigation');
			
			$('li.siwa-menu > a', whapp_html).click(function(ev) {
      	ev.preventDefault();
				if(!(whapp_html.hasClass('disabled'))) {
					$('#menu-siwa-navigation .siwa-menu > a').removeClass('activate');
					$(this).addClass('activate');

					$('.sub-menu').css('display','none');
				
					winkstart.publish($(this).attr('title') + '.activate', {});
        }
      });
			
			(whapp_html)
       	.hoverIntent({
         		sensitivity: 1,
            interval: 40,
            timeout: 200,
            over: function() {
            	if((whapp_html).dataset('dropdown') && !(whapp_html.hasClass('disabled'))) {
              	(whapp_html).addClass('open');
              }
            },
            out: function() {
            	if((whapp_html).dataset('dropdown')) {
              	(whapp_html).removeClass('open');
							}
            }
      	});

				
				
				var whapp_html = $('.menu-item-has-children'),
								whapp_dropdown_html = $('> .sub-menu', whapp_html),
                whapp_module_list_html = $('.module', whapp_dropdown_html),
                whapp_module_html = $('.module', whapp_dropdown_html),
                inserted = false,
                module_divider_html,
                category_html,
								curr_module,
								mod_data;

            THIS.ensure_dropdown(whapp_html);

            $(' > a', whapp_module_html).click(function(ev) {
                ev.preventDefault();
								
				
                $('#menu-siwa-navigation .siwa-menu > a').removeClass('activate');
                $(this).parents('.siwa-menu').find('a').addClass('activate');
								$('.sub-menu').css('display','none');
								
								$('.module').each(function(index, element) {
                  $(this).removeClass('focus');
									$(this).removeClass('open');
									$(this).removeClass('dropdown');
                });		
                winkstart.publish($(this).parents('.siwa-menu').find('a').attr('title') + '.module_activate', {name:$(this).attr('title')});
								
            });
						
						
    	},
			

    {
        add: function(args) {
            

            
        },

        disable_whapp: function(whapp_name) {
            var THIS = this,
                whapp_list_html = $(THIS.config.targets.nav_bar);

            $('li[data-whapp='+whapp_name+']', whapp_list_html).addClass('disabled');
        },

        enable_whapp: function(whapp_name) {
            var THIS = this,
                whapp_list_html = $(THIS.config.targets.nav_bar);

            $('li[data-whapp='+whapp_name+']', whapp_list_html).removeClass('disabled');
        },

        show_menu: function(whapp) {
            var THIS = this,
                whapp_list_html = $(THIS.config.targets.nav_bar),
                whapp_html = $('.whapp[data-whapp="' + whapp + '"]', whapp_list_html);

            if((whapp_html).dataset('dropdown')) {
                (whapp_html).addClass('open');
            }
        },

        hide_menu: function(whapp) {
            var THIS = this,
                whapp_list_html = $(THIS.config.targets.nav_bar),
                whapp_html = $('.whapp[data-whapp="' + whapp + '"]', whapp_list_html);

            if((whapp_html).dataset('dropdown')) {
                (whapp_html).removeClass('open');
            }
        },

        sub_add: function(data) {
            var THIS = this,
                whapp_list_html = $(THIS.config.targets.nav_bar),
                whapp_html = $('.whapp[data-whapp="' + data.whapp + '"]', whapp_list_html),
                whapp_dropdown_html = $('> .dropdown-menu', whapp_html),
                whapp_module_list_html = $('.module[data-category="' + (data.category || '') + '"]', whapp_dropdown_html),
                whapp_module_html = THIS.templates.module.tmpl(data),
                inserted = false,
                module_divider_html,
                category_html;

            THIS.ensure_dropdown(whapp_html);

            $('> a', whapp_module_html).click(function(ev) {
                ev.preventDefault();

                $('.whapps .whapp > a').removeClass('activate');
                $(this).parents('.whapp').find('a').addClass('activate');

                winkstart.publish(data.whapp + '.module_activate', { name: data.module });
            });

            (whapp_module_list_html).each(function(index) {
                var weight = $(this).dataset('weight');

                if(data.weight < weight) {
                    $(this).before(whapp_module_html);

                    inserted = true;

                    return false;
                }
                else if(index >= whapp_module_list_html.length - 1) {
                    $(this).after(whapp_module_html);

                    inserted = true;

                    return false;
                }
            });

            if(!inserted) {
                if(data.category) {
                    /* This should become its own function at somepoint... */
                    module_divider_html = THIS.templates.module_divider.tmpl();
                    category_html = THIS.templates.category.tmpl({
                        name: data.category,
                        label: data.category[0].toUpperCase() + data.category.slice(1),
                        icon: data.category
                    });

                    (category_html)
                        .hoverIntent({
                            sensitivity: 1,
                            interval: 40,
                            timeout: 200,
                            over: function() {
                                if((category_html).dataset('dropdown')) {
                                    (category_html).addClass('open');
                                }
                            },
                            out: function() {
                                if((category_html).dataset('dropdown')) {
                                    (category_html).removeClass('open');
                                }
                            }
                        });

                    $('.dropdown-menu', category_html).prepend(whapp_module_html);

                    (whapp_dropdown_html)
                        .append(category_html);
                }
                else {
                    (whapp_dropdown_html).prepend(whapp_module_html);
                }
            }

            /* Make sure all the sub menus are aligned correctly */
            $('.category > .dropdown-menu', whapp_dropdown_html).css('left', (whapp_dropdown_html).width());
        },

        ensure_dropdown: function(whapp_html) {
            $('> a', whapp_html).addClass('dropdown-toggle');

            (whapp_html)
                .addClass('dropdown')
                .dataset('dropdown', 'dropdown');
        }
    }
);
