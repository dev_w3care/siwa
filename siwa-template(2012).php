<?php
/**
 * Template Name: Full Width Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

?>
<?php get_header(); ?>

<div id="main-content" class="main-content background">



	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

		

		</div><!-- #content -->
	</div><!-- #primary -->
	<?php get_sidebar( 'content' ); ?>
</div><!-- #main-content -->

<?php
?>
<script type="text/javascript">
jQuery(document).ready(function(e) {
	var content_width = jQuery('#content').width();
	var main_width = jQuery('#main').width();
	
	var per = (content_width*100)/main_width;
	if(per<80){
		jQuery('#content').css('width','99%');
	}
	 
});

</script>
<?php
//get_sidebar();
get_footer();
?>
