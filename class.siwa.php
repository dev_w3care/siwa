<?php

class Siwa {
	
	const MENU_NAME = 'siwa-navigation';
	
	 public function set_navigation(){
    	$menu_exists = wp_get_nav_menu_object( Siwa::MENU_NAME );
	
			if( !$menu_exists){
				 wp_create_nav_menu(Siwa::MENU_NAME);
			}
    }
    
    function add_menu_items(){
    	$menu = wp_get_nav_menu_object( Siwa::MENU_NAME );

    	//echo 'menu id--'.$menu->ID;exit;
    	if($menu->term_id){

			$nav_items_to_add = array(
			        'developer' => array(
			            'title' => 'Developer',
			            'path' => 'developer',
			            ),
			        'siwa-accounts' => array(
			            'title' => 'Accounts',
			            'path' => 'accounts',
			            ),
			        'voip-services' => array(
			            'title' => 'VoIP Services',
			            'path' => 'voip',
			            ),
			        'pbx_connecter' => array(
			            'title' => 'Pbx Connecter',
			            'path' => 'pbxs',
			            ),
			        'number-manager' => array(
			            'title' => 'Number Manager',
			            'path' => 'numbers',
			            ),
			        'user-portal' => array(
			            'title' => 'User Portal',
			            'path' => 'userportal',
			            ),
			        'call-center' => array(
			            'title' => 'Call-Center',
			            'path' => 'call_center',
			            ),
			    );

			    $voip_items_to_add = array(
			        'account_details' => array(
			            'title' => 'Account Details',
			            'path' => 'account',
			            ),
			        'siwa-registrations' => array(
			            'title' => 'Registrations',
			            'path' => 'registration',
			            ),
			        'callflows' => array(
			            'title' => 'Callflows',
			            'path' => 'callflow',
			            ),
			        'featurecode' => array(
			            'title' => 'Feature Codes',
			            'path' => 'featurecode',
			            ),
			        'advance' => array(
			            'title' => 'Advance',
			            'path' => 'advance',
			            ),    
			            
			    );
			    
			    $voip_advance_items_to_add = array(
			        'conference' => array(
			            'title' => 'Conferences',
			            'path' => 'conference',
			            ),
			        'user' => array(
			            'title' => 'Users',
			            'path' => 'user',
			            ),
			        'resource' => array(
			            'title' => 'Carriers',
			            'path' => 'resource',
			            ),
			        'device' => array(
			            'title' => 'Devices',
			            'path' => 'device',
			            ),
			        'timeofday' => array(
			            'title' => 'Time Of Day',
			            'path' => 'timeofday',
			            ),
			        'vmbox' => array(
			            'title' => 'Voicemail Boxes',
			            'path' => 'vmbox',
			            ),
			        'menu' => array(
			            'title' => 'Menus',
			            'path' => 'menu',
			            ),
			        'media' => array(
			            'title' => 'Media',
			            'path' => 'media',
			            ),
			        'cdr' => array(
			            'title' => 'Call History',
			            'path' => 'cdr',
			            ),
			        'directory' => array(
			            'title' => 'Directory',
			            'path' => 'directory',
			            ),
			        'groups' => array(
			            'title' => 'Directories',
			            'path' => 'groups',
			            ),              
			        'bulk' => array(
			            'title' => 'Bulk Edit',
			            'path' => 'bulk',
			            ),    
			    );    
			    
			    $call_center_items_to_add = array(
			        'manage_queues ' => array(
			            'title' => 'Manage Queues',
			            'path' => 'queue',
			            ),
			        'dashboard' => array(
			            'title' => 'Dashboard',
			            'path' => 'dashboard',
			            )
			    );
			    
				 $i=0;
			foreach ( $nav_items_to_add as $slug => $nav_item ) {
				$new_menu_obj[$slug] = array();
			  $new_menu_obj[$slug]['id'] = wp_update_nav_menu_item($menu->term_id, 0,  array(
			    	'menu-item-position' => $i,
						'menu-item-type' => 'custom',
						'menu-item-title' => $nav_item['title'],
						'menu-item-url' => '#',
						'menu-item-description' => '',
						'menu-item-attr-title' => $nav_item['path'],
						'menu-item-target' => '',
						'menu-item-classes' => 'siwa-menu',
						'menu-item-status' => 'publish',
					)
			  );
			  
			  if($slug=='voip-services'){

			  	$j=0;
			  	foreach ( $voip_items_to_add as $voip_slug => $voip_item ) {
						$voip_menu_obj[$voip_slug] = array();
					  $voip_menu_obj[$voip_slug]['id'] = wp_update_nav_menu_item($menu->term_id, 0,  array(
					    	'menu-item-position' => $j,
								'menu-item-type' => 'custom',
								'menu-item-parent-id' => $new_menu_obj[$slug]['id'],
								'menu-item-title' => $voip_item['title'],
								'menu-item-url' => '#',
								'menu-item-description' => '',
								'menu-item-attr-title' => $voip_item['path'],
								'menu-item-target' => '',
								'menu-item-classes' => 'module',
								'menu-item-status' => 'publish',
							)
					  );
					  
					  if($voip_slug=='advance'){

					  	$k=0;
					  	foreach ( $voip_advance_items_to_add as $voip_advance_slug => $voip_advance_item ) {
								$voip_advance_menu_obj[$voip_slug] = array();
							  $voip_advance_menu_obj[$voip_slug]['id'] = wp_update_nav_menu_item($menu->term_id, 0,  array(
							    	'menu-item-position' => $k,
										'menu-item-type' => 'custom',
										'menu-item-parent-id' =>$voip_menu_obj[$voip_slug]['id'],
										'menu-item-title' => $voip_advance_item['title'],
										'menu-item-url' => '#',
										'menu-item-description' => '',
										'menu-item-attr-title' => $voip_advance_item['path'],
										'menu-item-target' => '',
										'menu-item-classes' => 'module',
										'menu-item-status' => 'publish',
									)
							  );
							  $k++;
					  	}  
					  }
					  
					  $j++;
			  	}  
			  }
			  if($slug=='call-center'){

			  	$j=0;
			  	foreach ( $call_center_items_to_add as $call_center_slug => $call_center_item ) {
						$call_center_menu_obj[$call_center_slug] = array();
					  $call_center_menu_obj[$call_center_slug]['id'] = wp_update_nav_menu_item($menu->term_id, 0,  array(
					    	'menu-item-position' => $j,
								'menu-item-type' => 'custom',
								'menu-item-parent-id' => $new_menu_obj[$slug]['id'],
								'menu-item-title' => $call_center_item['title'],
								'menu-item-url' => '#',
								'menu-item-description' => '',
								'menu-item-attr-title' => $call_center_item['path'],
								'menu-item-target' => '',
								'menu-item-classes' => 'module',
								'menu-item-status' => 'publish',
							)
					  );
					  $j++;
			  	}
			  }	  
				$i++;
			}
    	}
		}
		
		public function remove_navigation(){
    	$menu = wp_get_nav_menu_object( Siwa::MENU_NAME );
	
			if( $menu){
				  wp_delete_term( $menu->term_id, $menu->taxonomy);
			}
    }
    
    function remove_menu_items(){
    	global $wpdb;
			$menu = wp_get_nav_menu_object( Siwa::MENU_NAME );
			$results = $wpdb->get_results( "SELECT object_id FROM ".$wpdb->term_relationships." WHERE term_taxonomy_id = ".$menu->term_taxonomy_id);

			if(count($results)>0){
				foreach ($results as $result){
		 			wp_delete_post($result->object_id); 
				}
			}
		}
		
		function add_siwa_script(){
			if(is_page_template( 'siwa-template.php' )){
  	wp_register_script( 'top-js.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/top-js.js', array(), SIWA_VERSION );
		wp_enqueue_script( 'top-js.js' );
		print('<script type="text/javascript">var plugin_url = "'.SIWA__PLUGIN_URL.'kazoo-ui-master";</script>');
		print('<style type="text/css">#main {min-height: 500px;}</style>');
		
		wp_register_script( 'jquery-1.6.2.min.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/external/jquery-1.6.2.min.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'jquery-1.6.2.min.js' );
		wp_register_script( 'jquery-ui-1.8.7.custom.min.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/external/jquery-ui-1.8.7.custom.min.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'jquery-ui-1.8.7.custom.min.js' );
  	/*wp_register_style( 'bootstrap.min.css', SIWA__PLUGIN_URL . 'kazoo-ui-master/lib/bootstrap/css/bootstrap.min.css', array(), SIWA_VERSION );
		wp_enqueue_style( 'bootstrap.min.css');*/
		
		wp_register_script( 'bootstrap-alerts.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/lib/bootstrap/js/bootstrap-alerts.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'bootstrap-alerts.js' );
		wp_register_script( 'bootstrap-buttons.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/lib/bootstrap/js/bootstrap-buttons.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'bootstrap-buttons.js' );
		wp_register_script( 'bootstrap-modal.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/lib/bootstrap/js/bootstrap-modal.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'bootstrap-modal.js' );
		wp_register_script( 'bootstrap-twipsy.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/lib/bootstrap/js/bootstrap-twipsy.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'bootstrap-twipsy.js' );
		wp_register_script( 'bootstrap-popover.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/lib/bootstrap/js/bootstrap-popover.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'bootstrap-popover.js' );
		wp_register_script( 'bootstrap-scrollspy.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/lib/bootstrap/js/bootstrap-scrollspy.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'bootstrap-scrollspy.js' );
		wp_register_script( 'bootstrap-tabs.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/lib/bootstrap/js/bootstrap-tabs.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'bootstrap-tabs.js' );
		
		wp_register_style( 'validate.css', SIWA__PLUGIN_URL . 'kazoo-ui-master/css/validate.css', array(), SIWA_VERSION );
		wp_enqueue_style( 'validate.css');
		wp_register_style( 'tables.css', SIWA__PLUGIN_URL . 'kazoo-ui-master/css/tables.css', array(), SIWA_VERSION );
		wp_enqueue_style( 'tables.css');
		wp_register_style( 'jslider.css', SIWA__PLUGIN_URL . 'kazoo-ui-master/css/jslider/jslider.css', array(), SIWA_VERSION );
		wp_enqueue_style( 'jslider.css');
		wp_register_style( 'jquery-ui-1.8.8.custom.css', SIWA__PLUGIN_URL . 'kazoo-ui-master/css/ui-lightness/jquery-ui-1.8.8.custom.css', array(), SIWA_VERSION );
		wp_enqueue_style( 'jquery-ui-1.8.8.custom.css');
		
		wp_register_style( 'ui.listpanel.css', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/external/ui-listpanel/css/ui.listpanel.css', array(), SIWA_VERSION );
		wp_enqueue_style( 'ui.listpanel.css');
		wp_register_style( 'jquery.jscrollpane.css', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/external/ui-listpanel/css/jquery.jscrollpane.css', array(), SIWA_VERSION );
		wp_enqueue_style( 'jquery.jscrollpane.css');
		wp_register_style( 'jquery.jqplot.css', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/external/jqplot/css/jquery.jqplot.css', array(), SIWA_VERSION );
		wp_enqueue_style( 'jquery.jqplot.css');
		
		wp_register_style( 'jquery.tooltip.css', SIWA__PLUGIN_URL . 'kazoo-ui-master/css/jquery.tooltip.css', array(), SIWA_VERSION );
		wp_enqueue_style( 'jquery.tooltip.css');
		
		/*wp_register_style( 'kazoo.css', SIWA__PLUGIN_URL . 'kazoo-ui-master/css/kazoo.css', array(), SIWA_VERSION );
		wp_enqueue_style( 'kazoo.css');*/
		
		wp_register_script( 'jquery.cors.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/external/jquery.cors.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'jquery.cors.js' );
		wp_register_script( 'underscore-min.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/external/underscore-min.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'underscore-min.js' );
		wp_register_script( 'form2object.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/external/form2object.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'form2object.js' );
		wp_register_script( 'formToWizard.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/external/formToWizard.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'formToWizard.js' );
		wp_register_script( 'async.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/external/async.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'async.js' );
		wp_register_script( 'md5_generator.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/external/md5_generator.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'md5_generator.js' );
		wp_register_script( 'jstz.min.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/external/jstz.min.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'jstz.min.js' );
		wp_register_script( 'jquery.tmpl.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/external/tmpl/jquery.tmpl.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'jquery.tmpl.js' );
		wp_register_script( 'jquery.tmplPlus.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/external/tmpl/jquery.tmplPlus.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'jquery.tmplPlus.js' );
		wp_register_script( 'jquery.isObject.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/external/jquery.isObject.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'jquery.isObject.js' );
		wp_register_script( 'jquery.mousewheel.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/external/jquery.mousewheel.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'jquery.mousewheel.js' );
		wp_register_script( 'jquery.jscrollpane.min.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/external/jquery.jscrollpane.min.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'jquery.jscrollpane.min.js' );
		wp_register_script( 'jquery.hoverIntent.minified.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/external/jquery.hoverIntent.minified.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'jquery.hoverIntent.minified.js' );
		wp_register_script( 'jquery.dataTables.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/external/jquery.dataTables.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'jquery.dataTables.js' );
		wp_register_script( 'sort.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/external/plugins/sort.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'sort.js' );
		wp_register_script( 'jquery.slider.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/external/jquery.slider.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'jquery.slider.js' );
		wp_register_script( 'jquery.dependClass.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/external/jquery.dependClass.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'jquery.dependClass.js' );
		wp_register_script( 'jquery.dataset.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/external/jquery.dataset.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'jquery.dataset.js' );
		wp_register_script( 'ui.listpanel.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/external/ui-listpanel/ui.listpanel.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'ui.listpanel.js' );
		wp_register_script( 'lab.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/external/lab.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'lab.js' );
		wp_register_script( 'jquery.tooltip.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/external/jquery.tooltip.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'bootstrap-tabs.js' );
		wp_register_script( 'jquery.cookie.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/external/jquery.cookie.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'jquery.cookie.js' );
		wp_register_script( 'jquery.masonry.min.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/external/jquery.masonry.min.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'jquery.masonry.min.js' );
		wp_register_script( 'jquery.switch.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/external/jquery.switch.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'jquery.switch.js' );
		wp_register_script( 'jquery.jqplot.min.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/external/jqplot/jquery.jqplot.min.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'jquery.jqplot.min.js' );
		wp_register_script( 'jqplot.pieRenderer.min.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/external/jqplot/jqplot.pieRenderer.min.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'jqplot.pieRenderer.min.js' );
		
		wp_register_script( '_amplify.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/amplify/_amplify.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( '_amplify.js' );
		
		wp_register_script( 'amplify-var.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/amplify-var.js', array('jquery','_amplify.js'), SIWA_VERSION );
		wp_enqueue_script( 'amplify-var.js' );
		
		wp_register_script( 'amplify.core.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/amplify/amplify.core.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'amplify.core.js' );
		wp_register_script( 'amplify-core-var.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/amplify-core-var.js', array('jquery','amplify.core.js'), SIWA_VERSION );
		wp_enqueue_script( 'amplify-core-var.js' );
		wp_register_script( 'amplify.request.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/amplify/amplify.request.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'amplify.request.js' );
		wp_register_script( 'amplify.request.poll.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/amplify/amplify.request.poll.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'amplify.request.poll.js' );
		wp_register_script( 'load.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/config/load.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'load.js' );
		
		wp_register_script( 'winkstart-request.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/winkstart-request.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'winkstart-request.js' );
		wp_register_script( 'winkstart-util.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/winkstart-util.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'winkstart-util.js' );
		wp_register_script( 'winkstart-tables.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/winkstart-tables.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'winkstart-tables.js' );
		wp_register_script( 'winkstart-validate.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/winkstart-validate.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'winkstart-validate.js' );
		wp_register_script( 'winkstart-timezone.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/winkstart-timezone.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'winkstart-timezone.js' );
		wp_register_script( 'winkstart.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/winkstart.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'winkstart.js' );
		
		wp_register_script( 'url-for-template.js', SIWA__PLUGIN_URL . 'kazoo-ui-master/js/url-for-template.js', array('jquery'), SIWA_VERSION );
		wp_enqueue_script( 'url-for-template.js' );
			}
		
  }
  
  function siwa_filter_nav_menu($menu_items, $args){
    if(is_page_template( 'siwa-template.php' )){
			$menu_n = wp_get_nav_menu_object( Siwa::MENU_NAME );
			
			$args->menu = Siwa::MENU_NAME;
    	//return $menu;
    }
   
	}

			
}